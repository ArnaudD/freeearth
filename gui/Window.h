#ifndef WINDOW_H_INCLUDED
#define WINDOW_H_INCLUDED

#include <QtWidgets>
#include "gui/MdiSubWindowEdit.h"

class Scene;

class Window : public QMainWindow
{
	Q_OBJECT

	public:
		Window();

	private slots:
		void slot_newPlanet();
		void slot_loadPlanet();
		void slot_savePlanet();
		void slot_saveAsPlanet();
		
		void slot_edit();
		void slot_map();
		void slot_history();
		void slot_report();
		
		void slot_geosphere();
		void slot_atmosphere();
		void slot_biosphere();
		void slot_civilization();
		
		void slot_airSample();
		void slot_biomes();
		void slot_lifeForms();
		void slot_technology();
		
		void slot_speedFast();
		void slot_speedModerate();
		void slot_speedSlow();
		void slot_speedPause();
		
	private:
		QMenu* m_menuFile;
		QToolBar* m_toolbarFile;
		QAction* m_actionNewPlanet;
		QAction* m_actionLoadPlanet;
		QAction* m_actionSavePlanet;
		QAction* m_actionSaveAsPlanet;
		QAction* m_actionQuit;

		QMenu* m_menuWindows;
		QToolBar* m_toolbarWindows;
		QAction* m_actionEdit;
		QAction* m_actionMap;
		QAction* m_actionHistory;
		QAction* m_actionReport;
		
		QMenu* m_menuModels;
		QToolBar* m_toolbarModels;
		QAction* m_actionGeosphere;
		QAction* m_actionAtmosphere;
		QAction* m_actionBiosphere;
		QAction* m_actionCivilization;

		QMenu* m_menuGraphs;
		QToolBar* m_toolbarGraphs;
		QAction* m_actionAirSample;
		QAction* m_actionBiomes;
		QAction* m_actionLifeForms;
		QAction* m_actionTechnology;

		QMenu* m_menuSpeed;
		QToolBar* m_toolbarSpeed;
		QAction* m_actionSpeedFast;
		QAction* m_actionSpeedModerate;
		QAction* m_actionSpeedSlow;
		QAction* m_actionSpeedPause;
		
		// Windows
		QMdiArea* m_centralArea;
		
		MdiSubWindowEdit* m_mdiSubWindowEdit;
};

#endif

