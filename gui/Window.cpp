#include "gui/Window.h"

Window::Window() : QMainWindow()
{
	setWindowTitle("freeEarth");
	setMouseTracking(true);

	// Initialization menu File
	m_menuFile = menuBar()->addMenu("File");
	m_toolbarFile = addToolBar("File");

	m_actionNewPlanet = m_menuFile->addAction("New Planet");
	m_toolbarFile->addAction(m_actionNewPlanet);
	m_actionNewPlanet->setIcon(QIcon(":/resources/icons/fileNew.png"));
	connect(m_actionNewPlanet, SIGNAL(triggered()), this, SLOT(slot_newPlanet()));

	m_actionLoadPlanet = m_menuFile->addAction("Load Planet");
	m_toolbarFile->addAction(m_actionLoadPlanet);
	m_actionLoadPlanet->setIcon(QIcon(":/resources/icons/fileLoad.png"));
	connect(m_actionLoadPlanet, SIGNAL(triggered()), this, SLOT(slot_loadPlanet()));

	m_actionSavePlanet = m_menuFile->addAction("Save Planet");
	m_toolbarFile->addAction(m_actionSavePlanet);
	m_actionSavePlanet->setIcon(QIcon(":/resources/icons/fileSave.png"));
	connect(m_actionSavePlanet, SIGNAL(triggered()), this, SLOT(slot_savePlanet()));

	m_actionSaveAsPlanet = m_menuFile->addAction("Save Planet As");
	m_toolbarFile->addAction(m_actionSaveAsPlanet);
	m_actionSaveAsPlanet->setIcon(QIcon(":/resources/icons/fileSaveAs.png"));
	connect(m_actionSaveAsPlanet, SIGNAL(triggered()), this, SLOT(slot_saveAsPlanet()));

	m_actionQuit = m_menuFile->addAction("Quit");
	m_toolbarFile->addAction(m_actionQuit);
	m_actionQuit->setIcon(QIcon(":/resources/icons/fileQuit.png"));
	connect(m_actionQuit, SIGNAL(triggered()), qApp, SLOT(quit()));

	// Initialization menu Windows
	m_menuWindows = menuBar()->addMenu("Windows");
	m_menuWindows->setEnabled(false);
	m_toolbarWindows = addToolBar("Windows");
	m_toolbarWindows->hide();

	m_actionEdit = m_menuWindows->addAction("Edit");
	m_toolbarWindows->addAction(m_actionEdit);
	m_actionEdit->setIcon(QIcon(":/resources/icons/windowEdit.png"));
	connect(m_actionEdit, SIGNAL(triggered()), this, SLOT(slot_edit()));

	m_actionMap = m_menuWindows->addAction("Map");
	m_toolbarWindows->addAction(m_actionMap);
	m_actionMap->setIcon(QIcon(":/resources/icons/windowMap.png"));
	connect(m_actionMap, SIGNAL(triggered()), this, SLOT(slot_map()));

	m_actionHistory = m_menuWindows->addAction("History");
	m_toolbarWindows->addAction(m_actionHistory);
	m_actionHistory->setIcon(QIcon(":/resources/icons/windowHistory.png"));
	connect(m_actionHistory, SIGNAL(triggered()), this, SLOT(slot_history()));

	m_actionReport = m_menuWindows->addAction("Report");
	m_toolbarWindows->addAction(m_actionReport);
	m_actionReport->setIcon(QIcon(":/resources/icons/windowReport.png"));
	connect(m_actionReport, SIGNAL(triggered()), this, SLOT(slot_report()));

	// Initialization menu Models
	m_menuModels = menuBar()->addMenu("Models");
	m_menuModels->setEnabled(false);
	m_toolbarModels = addToolBar("Models");
	m_toolbarModels->hide();

	m_actionGeosphere = m_menuModels->addAction("Geosphere");
	m_toolbarModels->addAction(m_actionGeosphere);
	m_actionGeosphere->setIcon(QIcon(":/resources/icons/modelGeosphere.png"));
	connect(m_actionGeosphere, SIGNAL(triggered()), this, SLOT(slot_geosphere()));

	m_actionAtmosphere = m_menuModels->addAction("Atmosphere");
	m_toolbarModels->addAction(m_actionAtmosphere);
	m_actionAtmosphere->setIcon(QIcon(":/resources/icons/modelAtmosphere.png"));
	connect(m_actionAtmosphere, SIGNAL(triggered()), this, SLOT(slot_atmosphere()));

	m_actionBiosphere = m_menuModels->addAction("Biosphere");
	m_toolbarModels->addAction(m_actionBiosphere);
	m_actionBiosphere->setIcon(QIcon(":/resources/icons/modelBiosphere.png"));
	connect(m_actionBiosphere, SIGNAL(triggered()), this, SLOT(slot_biosphere()));

	m_actionCivilization = m_menuModels->addAction("Civilization");
	m_toolbarModels->addAction(m_actionCivilization);
	m_actionCivilization->setIcon(QIcon(":/resources/icons/modelCivilization.png"));
	connect(m_actionCivilization, SIGNAL(triggered()), this, SLOT(slot_civilization()));
	
	// Initialization menu Graphs
	m_menuGraphs = menuBar()->addMenu("Graphs");
	m_menuGraphs->setEnabled(false);
	m_toolbarGraphs = addToolBar("Graphs");
	m_toolbarGraphs->hide();

	m_actionAirSample = m_menuGraphs->addAction("Air Sample");
	m_toolbarGraphs->addAction(m_actionAirSample);
	m_actionAirSample->setIcon(QIcon(":/resources/icons/graphAirSample.png"));
	connect(m_actionAirSample, SIGNAL(triggered()), this, SLOT(slot_airSample()));

	m_actionBiomes = m_menuGraphs->addAction("Biomes");
	m_toolbarGraphs->addAction(m_actionBiomes);
	m_actionBiomes->setIcon(QIcon(":/resources/icons/graphBiomes.png"));
	connect(m_actionBiomes, SIGNAL(triggered()), this, SLOT(slot_biomes()));

	m_actionLifeForms = m_menuGraphs->addAction("Life-Forms");
	m_toolbarGraphs->addAction(m_actionLifeForms);
	m_actionLifeForms->setIcon(QIcon(":/resources/icons/graphLifeForms.png"));
	connect(m_actionLifeForms, SIGNAL(triggered()), this, SLOT(slot_lifeForms()));
	
	m_actionTechnology = m_menuGraphs->addAction("Technology");
	m_toolbarGraphs->addAction(m_actionTechnology);
	m_actionTechnology->setIcon(QIcon(":/resources/icons/graphTechnology.png"));
	connect(m_actionTechnology, SIGNAL(triggered()), this, SLOT(slot_technology()));
	
	// Initialization menu Speed
	m_menuSpeed = menuBar()->addMenu("Speed");
	m_menuSpeed->setEnabled(false);
	m_toolbarSpeed = addToolBar("Speed");
	m_toolbarSpeed->hide();
	
	m_actionSpeedFast = m_menuSpeed->addAction("Fast");
	m_toolbarSpeed->addAction(m_actionSpeedFast);
	m_actionSpeedFast->setIcon(QIcon(":/resources/icons/speedFast.png"));
	connect(m_actionSpeedFast, SIGNAL(triggered()), this, SLOT(slot_speedFast()));
	
	m_actionSpeedModerate = m_menuSpeed->addAction("Moderate");
	m_toolbarSpeed->addAction(m_actionSpeedModerate);
	m_actionSpeedModerate->setIcon(QIcon(":/resources/icons/speedModerate.png"));
	connect(m_actionSpeedFast, SIGNAL(triggered()), this, SLOT(slot_speedModerate()));

	m_actionSpeedSlow = m_menuSpeed->addAction("Slow");
	m_toolbarSpeed->addAction(m_actionSpeedSlow);
	m_actionSpeedSlow->setIcon(QIcon(":/resources/icons/speedSlow.png"));
	connect(m_actionSpeedFast, SIGNAL(triggered()), this, SLOT(slot_speedSlow()));
		
	m_actionSpeedPause = m_menuSpeed->addAction("Pause");
	m_toolbarSpeed->addAction(m_actionSpeedPause);
	m_actionSpeedPause->setIcon(QIcon(":/resources/icons/speedPause.png"));
	connect(m_actionSpeedFast, SIGNAL(triggered()), this, SLOT(slot_speedPause()));
	
	// Initialization central area
	m_centralArea = new QMdiArea(this);
	setCentralWidget(m_centralArea);	
}

void Window::slot_newPlanet()
{
	m_menuWindows->setEnabled(true);
	m_menuModels->setEnabled(true);
	m_menuGraphs->setEnabled(true);
	m_menuSpeed->setEnabled(true);
	
	m_toolbarWindows->show();
	m_toolbarModels->show();
	m_toolbarGraphs->show();
	m_toolbarSpeed->show();
		
	m_mdiSubWindowEdit = new MdiSubWindowEdit(this);
	m_centralArea->addSubWindow(m_mdiSubWindowEdit);	
	m_mdiSubWindowEdit->show();
}

void Window::slot_loadPlanet()
{
}

void Window::slot_savePlanet()
{
}

void Window::slot_saveAsPlanet()
{
}

void Window::slot_edit()
{
}

void Window::slot_map()
{
}

void Window::slot_history()
{
}

void Window::slot_report()
{
}

void Window::slot_geosphere()
{
}

void Window::slot_atmosphere()
{
}

void Window::slot_biosphere()
{
}

void Window::slot_civilization()
{
}

void Window::slot_airSample()
{
}

void Window::slot_biomes()
{
}

void Window::slot_lifeForms()
{
}

void Window::slot_technology()
{
}

void Window::slot_speedFast()
{
}

void Window::slot_speedModerate()
{
}

void Window::slot_speedSlow()
{
}

void Window::slot_speedPause()
{
}
