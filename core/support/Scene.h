#ifndef SCENE_H_INCLUDED
#define SCENE_H_INCLUDED

#include <QtWidgets>
#include "gui/View.h"

class MdiSubWindowEdit;

class Scene : public QGraphicsScene
{
	Q_OBJECT

	public:
		Scene(MdiSubWindowEdit*);
		~Scene();
		
	public slots:
		
	protected:
		virtual void mouseReleaseEvent (QGraphicsSceneMouseEvent*);

	private slots:

	private:
		MdiSubWindowEdit *m_parentWindow;

};

#endif
